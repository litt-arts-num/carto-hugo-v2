# carto-hugo v2

## Prérequis
* git
* composer (backend deps)
* npm (front deps)
* saxonb-xslt

## Installation

* clone repository
```
git clone ...
```

* install dependencies
```
composer install
npm install
```

* launch xsl
See: data/xslt/README.md
