<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

    <xsl:strip-space elements="*"/>
    <xsl:preserve-space elements="tei:name tei:ab tei:bibl tei:note tei:title"/>

    <!-- Template qui s'applique à la racine du XML -->
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:TEI">
        <xsl:result-document href="../../auto/htm/home.htm" method="xhtml" indent="yes">
            <xsl:apply-templates select="tei:teiHeader/tei:encodingDesc/tei:projectDesc"/>
        </xsl:result-document>
        <xsl:result-document href="../../auto/htm/about.htm" method="xhtml" indent="yes">
            <xsl:apply-templates select="tei:teiHeader/tei:encodingDesc/tei:projectDesc"/>
            <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:titleStmt" mode="team"/>
            <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:publicationStmt"/>
            <xsl:apply-templates select="tei:teiHeader/tei:fileDesc/tei:sourceDesc"/>
        </xsl:result-document>
        <xsl:result-document href="../../auto/htm/edition.htm" method="xhtml" indent="yes">
            <xsl:apply-templates select="tei:teiHeader/tei:encodingDesc/tei:editorialDecl"/>
        </xsl:result-document>
        <!--
                    <style>
            ul.dashed-list {
                list-style-type: none;
            }
            ul.dashed-list li::before {
                content: '\2014';
                position: absolute;
                margin-left: -20px;
            }</style>
        <xsl:apply-templates/>
        -->
    </xsl:template>

    <!-- Template autofermant qui permet, pour un élément donné, de ne rien faire -->
    <xsl:template match="tei:text"/>
    <xsl:template match="tei:facsimile"/>
    <xsl:template match="tei:teiHeader">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:profileDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:fileDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:titleStmt" mode="team">
        <h2 id="team">L'équipe</h2>
        <ul>
            <li>Responsable du projet : <xsl:apply-templates select="tei:principal" mode="titleStmt"
                /></li>
            <xsl:apply-templates select="tei:respStmt"/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:title" mode="titleStmt">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:author" mode="titleStmt">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:principal" mode="titleStmt">
        <xsl:apply-templates mode="titleStmt"/>
    </xsl:template>
    <xsl:template match="tei:respStmt">
        <li>
            <xsl:apply-templates select="tei:resp" mode="titleStmt"/>
            <xsl:text> : </xsl:text>
            <xsl:for-each select="tei:name">
                <xsl:if test="last() and not(position() = 1)">
                    <xsl:text> et </xsl:text>
                </xsl:if>
                <xsl:if test="not(last()) and not(position() = 1)">
                    <xsl:text>, </xsl:text>
                </xsl:if>
                <xsl:apply-templates select="." mode="titleStmt"/>
                <xsl:if test="last()">
                    <xsl:text>.</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </li>
    </xsl:template>
    <xsl:template match="tei:name" mode="titleStmt">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:forename">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:surname">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:affiliation">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:langUsage"/>
    <xsl:template match="tei:langUsage" mode="dont_use_it">
        <h2 id="lang">Langues</h2>
        Les langues présentes dans cette source sont :
            <ul><xsl:apply-templates/></ul>
    </xsl:template>
    <xsl:template match="tei:language">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:encodingDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:projectDesc">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:publicationStmt">
        <h2 id="website">Site</h2>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:authority">
        Ce site est publié par l'<xsl:apply-templates/>.
    </xsl:template>
    <xsl:template match="tei:availability">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:sourceDesc">
        <h2 id="sources">Sources</h2>
        <ul><xsl:apply-templates/></ul>
    </xsl:template>
    <xsl:template match="tei:bibl">
        <li><xsl:apply-templates/></li>
    </xsl:template>
    <xsl:template match="tei:author">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:listPlace"/>
    <xsl:template match="tei:editorialDecl">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:revisionDesc">
        <h2>Notes de version</h2>
        <xsl:apply-templates/>
    </xsl:template>

    <!-- Template qui permet pour chaque élément tei:p, de générer un élément HTML <p/>, 
        puis, concernant le contenu, d'appliquer les autres templates -->
    <xsl:template match="tei:p">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:lb">
        <br/>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'bold']">
        <b>
            <xsl:apply-templates/>
        </b>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'title']">
        <h2>
            <xsl:apply-templates/>
        </h2>
    </xsl:template>

    <xsl:template match="tei:list[@rend = 'dash']">
        <ul class="dashed-list">
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:list[@type = 'bulleted']">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template match="tei:title[@level = 'm']">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="tei:placeName">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:date">
        <xsl:apply-templates/>
    </xsl:template>
    
    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:value-of select="name(.)"/>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
