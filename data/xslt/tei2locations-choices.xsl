<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="xs tei" version="1.0">
  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="yes"/>

  <!--
        This XSL use a XML-TEI input file and generate an HTML list of location based on <tei:surface/>)
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
    -->
  <xsl:variable name="DEBUG">0</xsl:variable>

  <xsl:template name="generate_options">
    <xsl:variable name="this_placeName">
      <xsl:choose>
        <xsl:when test=" parent::tei:place">
          <xsl:value-of select="parent::tei:place/tei:placeName[1]"/>
          <xsl:text> > </xsl:text>
        </xsl:when>
      </xsl:choose>
      <xsl:for-each select="tei:placeName">
        <xsl:if test="position() > 1">
          <xsl:text> ou </xsl:text>
        </xsl:if>
        <xsl:value-of select="."/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="data_locations">
      <xsl:value-of select="@xml:id"/>
      <xsl:for-each select="tei:place">
        <xsl:text> </xsl:text>
        <xsl:value-of select="@xml:id"/>
      </xsl:for-each>
    </xsl:variable>
    <option data-location="{@xml:id}" data-locations="{$data_locations}">
      <xsl:attribute name="value">
        <xsl:value-of select="$this_placeName"/>
      </xsl:attribute>
      <xsl:value-of select="$this_placeName"/>
    </option>
    <xsl:for-each select="tei:place">
      <xsl:call-template name="generate_options"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="/tei:TEI">
    <datalist id="locations-choices">
      <xsl:for-each select="tei:teiHeader//tei:listPlace/tei:place">
        <xsl:call-template name="generate_options"/>
        <!--
        <xsl:variable name="this_placeName">
          <xsl:for-each select="tei:placeName">
            <xsl:if test="position() > 1">
              <xsl:text> ou </xsl:text>
            </xsl:if>
            <xsl:value-of select="."/>
          </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="data_locations">
          <xsl:value-of select="@xml:id"/>
          <xsl:for-each select="tei:place">
            <xsl:text> </xsl:text>
            <xsl:value-of select="@xml:id"/>
          </xsl:for-each>
        </xsl:variable>
        <option data-location="{@xml:id}" data-locations="{$data_locations}">
          <xsl:attribute name="value">
            <xsl:choose>
              <xsl:when test=".">
                <xsl:value-of select="$this_placeName"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="@xml:id"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
          <xsl:value-of select="$this_placeName"/>
        </option>
        <xsl:for-each select="tei:place">
          <xsl:for-each select="tei:placeName">
            <option data-location="{../@xml:id}">
              <xsl:variable name="value">
                <xsl:value-of select="../../tei:placeName[1]"/>
                <xsl:text> > </xsl:text>
                <xsl:choose>
                  <xsl:when test=".">
                    <xsl:value-of select="."/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="../@xml:id"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <xsl:attribute name="value">
                <xsl:value-of select="$value"/>
              </xsl:attribute>
              <xsl:text>  </xsl:text>
              <xsl:value-of select="$value"/>
            </option>
          </xsl:for-each>
        </xsl:for-each>-->
      </xsl:for-each>
    </datalist>
  </xsl:template>
</xsl:stylesheet>
