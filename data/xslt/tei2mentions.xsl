<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">
    <xsl:output method="text" indent="no" encoding="UTF-8" version="draft"
        omit-xml-declaration="yes"/>

    <!--
        This XSL use a XML-TEI input file and generate an JSON list of location's mention (based on `tei:surface/graphic/zone`)
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
    -->

    <xsl:variable name="DEBUG">0</xsl:variable>

    <xsl:template match="/tei:TEI">
        <xsl:text>{</xsl:text>
        <xsl:for-each select="tei:facsimile/tei:surface">
            <xsl:text>"</xsl:text>
            <xsl:value-of select="@xml:id"/>
            <xsl:text>": {</xsl:text>
            <xsl:for-each select="tei:zone">
                <xsl:text>"</xsl:text>
                <xsl:value-of select="substring-after(@corresp, '#')"/>
                <xsl:text>": [</xsl:text>
                <xsl:text>["</xsl:text>
                <xsl:value-of select="@ulx"/>
                <xsl:text>","</xsl:text>
                <xsl:value-of select="@uly"/>
                <xsl:text>","</xsl:text>
                <xsl:value-of select="@lrx - @ulx"/>
                <xsl:text>","</xsl:text>
                <xsl:value-of select="@lry - @uly"/>
                <xsl:text>"]</xsl:text>
                <xsl:text>]</xsl:text>
                <xsl:if test="position() != last()">
                    <xsl:text>,</xsl:text>
                </xsl:if>
            </xsl:for-each>
            <xsl:text>}</xsl:text>
            <xsl:if test="position() != last()">
                <xsl:text>,</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>}</xsl:text>
    </xsl:template>
</xsl:stylesheet>
