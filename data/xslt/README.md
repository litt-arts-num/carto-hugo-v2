# Files and folders organisation
```bash
index.php
template/ #twig templates
assets/   #js/ img/ css/
data/     #tei/ xslt/ auto/ (dont README.md)
                          svg/
						  js/mentions.json
						  htm/
```

# XSLT files
* tei2gallery.xsl : Generate the image gallery - (`<div id="gallery" style="column-count:2"/>`)
* tei2html.xsl : Generate the web editions of the source (`<div id="roman" class="card-body"/>`)
* tei2locations-choices.xsl : Generate the list of available locations (`<datalist id="locations-choices"/>` and its `<option/>`)
* tei2svg.xsl : Generate svg files
* tei2mentions.xsl : Generate mention.json - useful for galley view

# Useful command line
```bash
# from the data/xslt/ directory
xsltproc -o ../auto/htm/gallery.htm   ./tei2gallery.xsl ../tei/20210216_travailleursdelamer.xml
xsltproc -o ../auto/htm/roman.htm     ./tei2html.xsl    ../tei/20210216_travailleursdelamer.xml 
xsltproc -o ../auto/htm/locations-choices.htm ./tei2locations-choices.xsl ../tei/20210216_travailleursdelamer.xml 
xsltproc -o ../auto/js/mentions.json  ./tei2mentions.xsl ../tei/20210216_travailleursdelamer.xml 
saxonb-xslt  -ext:on ../tei/20210216_travailleursdelamer.xml ./tei2svg.xsl > ../auto/svg/tei2svg.log
```
