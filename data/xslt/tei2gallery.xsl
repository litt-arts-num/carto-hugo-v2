<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="1.0">
    <xsl:output method="html" indent="yes" encoding="UTF-8" version="draft" omit-xml-declaration="yes"/>

    <!--
        This XSL use a XML-TEI input file and generate an HTML gallery of images (based on <tei:surface/>)
        It has been created within the Carto Hugo project by AnneGF@CNRS.
        Feel free to use, reuse and CITE US ! ;-)
    -->

    <xsl:variable name="DEBUG">0</xsl:variable>

    <xsl:template match="/tei:TEI">
        <div id="gallery" style="column-count:2">
            <xsl:for-each select="tei:facsimile/tei:surface">
                <div class="card mb-2">
                    <div class="card-header">
                        <a class="image-modal" href="#" data-imagename="{@xml:id}" data-url="data/images/{@xml:id}">
                            <xsl:value-of select="@xml:id"/>
                        </a>
                    </div>
                    <div class="card-body p-0">
                        <object id="{@xml:id}" class="emb" data="data/auto/svg/{@xml:id}.svg" width="100%" type="image/svg+xml"></object>
                    </div>
                </div>
            </xsl:for-each>
        </div>
    </xsl:template>
</xsl:stylesheet>
