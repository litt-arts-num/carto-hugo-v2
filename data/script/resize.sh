#!/usr/bin/env bash
# Purpose: batch image resizer
# Source: https://guides.wp-bullet.com
# Author: Mike

# absolute path to image folder. Attention ça écrase les fichiers
FOLDER="/var/www/html/carto-hugo-v2/data/thumbnails"

# max width
WIDTH=1024

# max height
HEIGHT=1024

#resize png or jpg to either height or width, keeps proportions using imagemagick
find ${FOLDER} -type f -iname '*.jpeg' -exec convert \{} -verbose -resize $WIDTHx$HEIGHT\> \{} \;

#resize png to either height or width, keeps proportions using imagemagick
find ${FOLDER} -type f -iname '*.png' -exec convert \{} -verbose -resize $WIDTHx$HEIGHT\> \{} \;

#resize jpg/jpeg/png to either height or width, keeps proportions using imagemagick
find ${FOLDER} -type f -iname '*.jpg' -exec convert \{} -verbose -resize $WIDTHx$HEIGHT\> \{} \;
