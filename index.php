<?php

require_once __DIR__.'/bootstrap.php';

$page = (isset($_GET["page"])) ? $_GET["page"] : "index" ;

echo $twig->render('templates/'. $page .'.html.twig');
