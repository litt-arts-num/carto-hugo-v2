import {
  handler
} from './handler.js'

let viewer = OpenSeadragon({
  id: "seadragon-viewer",
  showNavigator: true,
  showRotationControl: true,
  prefixUrl: '',
  zoomInButton: 'osd-zoom-in',
  zoomOutButton: 'osd-zoom-out',
  homeButton: 'osd-home',
  fullPageButton: 'osd-full-page',
  nextButton: 'osd-next',
  previousButton: 'osd-previous',
  rotateLeftButton: 'osd-left',
  rotateRightButton: 'osd-right',
});

viewer.innerTracker.keyHandler = null

let zoom = false
let realCoordToZoom

const makeOverlay = function(i, mention, viewport, currentLocation, location) {
  let x = parseInt(mention[0], 10)
  let y = parseInt(mention[1], 10)
  let width = parseInt(mention[2], 10)
  let height = parseInt(mention[3], 10)
  let openseaCoord = new OpenSeadragon.Rect(x, y, width, height)
  let realCoord = viewport.imageToViewportRectangle(openseaCoord)
  let locationMatch = (currentLocation == location) ? true : false
  let elt = document.createElement("g")
  elt.className = (locationMatch) ? "overlay-active" : "overlay"
  elt.title = location
  elt.id = "overlay" + i
  elt.style.zIndex = 9999999;
  viewer.addOverlay({
    element: elt,
    location: realCoord
  })

  let tracker = new OpenSeadragon.MouseTracker({
    element: elt.id,
    clickHandler: function(event) {
      handler.toggleLocation(location);
    }
  })

  if (locationMatch && !zoom) {
    realCoordToZoom = realCoord
    zoom = true
  }
}

async function getData() {
  const response = await fetch("data/auto/js/mentions.json");
  return response.json();
}

viewer.addHandler('open', function() {
  let viewport = viewer.viewport
  let currentLocation = handler.currentLocationId
  let currentImage = handler.currentImage
  let overlay = viewer.svgOverlay();

  getData().then((data) => {
    let jsonMention = data
    zoom = false;
    let i = 0;
    if (jsonMention.hasOwnProperty(currentImage)) {
      for (var location in jsonMention[currentImage]) {
        i++
        if (Object.prototype.hasOwnProperty.call(jsonMention[currentImage], location)) {
          jsonMention[currentImage][location].forEach(function(mention) {
            makeOverlay(i, mention, viewport, currentLocation, location)
          })
          if (zoom) {
            viewport.fitBounds(realCoordToZoom, true)
          }
        }
      }
    }

    $(window).resize(function() {
      overlay.resize();
    });
  })

});

export {
  viewer
}