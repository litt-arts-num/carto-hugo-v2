let romanHandler = {
  container: document.getElementById("roman-container"),
  modal: $('#wait-modal'),

  getSize: function() {
    let regexp = this.container.className.match(/(\d+)/)
    return parseInt(regexp[1], 10)
  },
  openModal: function() {
    return new Promise((resolve, reject) => {
      this.modal.modal('show')
      setTimeout(function() {
        resolve('foo')
      }, 10)
    })
  },

  closeModal: function() {
    this.modal.modal('hide')
  },

  collapse: function() {
    let newSize = this.getSize() - 1
    if (newSize > 2) {
      this.openModal().then(() => {
          this.container.className = "col-" + newSize
        })
        .then(() => {
          this.closeModal()
        })
    }
  },

  expan: function() {
    let newSize = this.getSize() + 1
    if (newSize < 7) {
      this.openModal().then(() => {
          this.container.className = "col-" + newSize
        })
        .then(() => {
          this.closeModal()
        })
    }
  },

  switch: function(){
    this.container.classList.toggle("v2")
    document.getElementById("switch").innerHTML = (this.container.classList.contains("v2")) ? 'version avec additions' : 'version édition originale'

  }
}

export {
  romanHandler
}
