$('.note-call').click(function() {
  var noteModale = $("#noteModale");
  var id = $(this).data("note-id");
  var text = $("[data-note-content-id='" + id + "']").first().html();
  noteModale.find(".modal-body").html(text);
  noteModale.modal('show');
});
